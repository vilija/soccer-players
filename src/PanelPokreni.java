import java.awt.Color;

import javax.swing.*;

public class PanelPokreni extends JPanel {
	
	public int buttonW = 150;
	public int buttonH = 40;
	public int offsetW = buttonW + 50;
	public int inputW = 200;
	public int inputH = 40;
	public int offsetH = inputH + 20;
	
	JButton pokreniButton;
	Frejm frejm;

	public PanelPokreni() {
		setLayout(null);
		
		this.pokreniButton = new JButton("Pokreni");
		pokreniButton.setBounds(500-(buttonW/2), 400-(buttonH/2), buttonW, buttonH);
		pokreniButton.setBackground(Color.decode("#11144c"));
		pokreniButton.setForeground(Color.white);
		pokreniButton.setOpaque(true);
		this.add(pokreniButton);
		
		
		
	}
}
