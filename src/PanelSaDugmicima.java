import java.awt.Color;

import javax.swing.*;

public class PanelSaDugmicima extends JPanel {
	
	public int buttonW = 150;
	public int buttonH = 40;
	public int offsetW = buttonW + 30;
	public int inputW = 200;
	public int inputH = 40;
	public int offsetH = inputH + 20;
	
	public Frejm frejm;
	public JButton dodajButton;
	public JButton prikaziButton;
	public JButton nazadButton;
	public JButton ucitajButton;
	
	public PanelSaDugmicima() {
		
		setLayout(null);
		setVisible(false);
		
		nazadButton = new JButton("Nazad na pocetnu");
		nazadButton.setBounds(5, 5, buttonW, buttonH);
		nazadButton.setBackground(Color.decode("#0066ff"));
		nazadButton.setForeground(Color.white);
		this.add(nazadButton);
		
		dodajButton = new JButton("Dodaj");
		dodajButton.setBounds(nazadButton.getX() + offsetW, nazadButton.getY(), buttonW, buttonH);
		dodajButton.setBackground(Color.decode("#0066ff"));
		dodajButton.setForeground(Color.white);
		this.add(dodajButton);
		
		prikaziButton = new JButton("Prikazi");
		prikaziButton.setBounds(dodajButton.getX() + offsetW, dodajButton.getY(), buttonW, buttonH);
		prikaziButton.setBackground(Color.decode("#0066ff"));
		prikaziButton.setForeground(Color.white);
		this.add(prikaziButton);
		
		ucitajButton = new JButton("Ucitaj");
		ucitajButton.setBounds(prikaziButton.getX() + offsetW, prikaziButton.getY(), buttonW, buttonH);
		ucitajButton.setBackground(Color.decode("#0066ff"));
		ucitajButton.setForeground(Color.white);
		this.add(ucitajButton);
		
	}

}
