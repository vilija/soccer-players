import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class OsluskivacZaUcitaj implements ActionListener {

	public Frejm frejm;

	public OsluskivacZaUcitaj(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			frejm.fudbalskiKlub = new FudbalskiKlub();
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/fudbaleri", "root", "");

			String query = "SELECT `fudbaler`.`id` AS 'ID', `fudbaler`.`ime`, `fudbaler`.`godiste`, `menadzer`.`ime` AS 'menadzer', `prvi_klub`.`ime` AS 'prvi_klub' FROM `fudbaler`, `menadzer`, `prvi_klub` WHERE `fudbaler`.`menadzer_id` = `menadzer`.`id` AND `fudbaler`.`prvi_klub_id` = `prvi_klub`.`id`";

			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();

			resultSet.last();
			int brEl = resultSet.getRow();
			resultSet.beforeFirst();

			String id, ime, godiste, menadzer, prviKlub, pozicija;
			Fudbaler fudbaler;

			for (int i = 0; i < brEl; i++) {
				resultSet.next();
				id = resultSet.getString("id");
				ime = resultSet.getString("ime");
				godiste = resultSet.getString("godiste");
				menadzer = resultSet.getString("menadzer");
				prviKlub = resultSet.getString("prvi_klub");

				fudbaler = new Fudbaler(ime, godiste, menadzer, 20, prviKlub);

				query = "SELECT `pozicija`.`naziv_pozicije` AS 'pozicija' FROM `pozicija`, `igra` WHERE `igra`.`pozicija_id` = `pozicija`.`id` AND `fudbaler_id`= ?";

				preparedStatement = connection.prepareStatement(query);
				preparedStatement.setString(1, id);
				ResultSet resultSetFudbaleri = preparedStatement.executeQuery();

				while (resultSetFudbaleri.next()) {
					fudbaler.push(resultSetFudbaleri.getString("pozicija"));
				}

				frejm.fudbalskiKlub.dodaj(fudbaler);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
