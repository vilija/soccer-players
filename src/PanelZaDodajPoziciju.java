import javax.swing.*;

public class PanelZaDodajPoziciju extends JPanel {
	
	public int buttonW = 150;
	public int buttonH = 40;
	public int offsetW = buttonW + 100;
	public int inputW = 200;
	public int inputH = 40;
	public int offsetH = inputH + 20;

	public JLabel pozicijaJLabel;
	public JTextField pozicijaField;
	public JButton dodajButton;
	public JButton gotovoButton;
	public Frejm frejm;
	

	public PanelZaDodajPoziciju(Frejm frejm) {
		this.frejm = frejm;
		this.setLayout(null);
		
		pozicijaJLabel = new JLabel("Pozicija");
		pozicijaJLabel.setBounds(20, 50, inputW, inputH);
		this.add(pozicijaJLabel);
		
		pozicijaField = new JTextField();
		pozicijaField.setBounds(pozicijaJLabel.getX() + offsetW, pozicijaJLabel.getY(), inputW, inputH);
		this.add(pozicijaField);
		
		dodajButton = new JButton("Dodaj poziciju");
		dodajButton.setBounds(pozicijaJLabel.getX(), pozicijaJLabel.getY() + offsetH, buttonW, buttonH);
		this.add(dodajButton);
		
		gotovoButton = new JButton("Gotovo");
		gotovoButton.setBounds(dodajButton.getX() + offsetW, dodajButton.getY(), buttonW, buttonH);
		this.add(gotovoButton);
	}
}
