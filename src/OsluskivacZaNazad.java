import java.awt.event.*;

public class OsluskivacZaNazad implements ActionListener {

	public Frejm frejm;

	public OsluskivacZaNazad(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.panelSaDugmicima.setVisible(false);
		frejm.panelDodaj.setVisible(false);
		frejm.panelPokreni.setVisible(true);
		frejm.panelZaDodajPoziciju.setVisible(false);

	}

}
