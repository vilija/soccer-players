
public class Fudbaler {
	private String imeIPrezime;
	private String godinaRodjenja;
	private String menadzer;
	private String[] pozicija;
	private String prviKlub;
	private int trenutniBroj;
	private int brojPozicija;

	public Fudbaler() {

	}

	public Fudbaler(String imeIPrezime, String godinaRodjenja, String menadzer, String[] pozicija, String prviKlub) {
		this.imeIPrezime = imeIPrezime;
		this.godinaRodjenja = godinaRodjenja;
		this.menadzer = menadzer;
		this.prviKlub = prviKlub;
		this.pozicija = new String[pozicija.length];
		this.brojPozicija = this.trenutniBroj = pozicija.length;
		for (int i = 0; i < pozicija.length; i++) {
			this.pozicija[i] = pozicija[i];
		}
	}

	public Fudbaler(String imeIPrezime, String godinaRodjenja, String menadzer, int brojPozicije, String prviKlub) {
		this.imeIPrezime = imeIPrezime;
		this.godinaRodjenja = godinaRodjenja;
		this.menadzer = menadzer;
		this.prviKlub = prviKlub;
		this.trenutniBroj = 0;
		this.brojPozicija = brojPozicije;
		this.pozicija = new String[this.brojPozicija];

	}

	public void push(String pozicija) {
		if (this.trenutniBroj < this.brojPozicija) {
			this.pozicija[this.trenutniBroj++] = pozicija;
		} else {
			System.out.println("Niz je pun");
		}
	}

	public String getImeIPrezime() {
		return this.imeIPrezime;
	}

	public String getGodinaRodjenja() {
		return this.godinaRodjenja;
	}

	public String getMenadzer() {
		return this.menadzer;
	}

	public String[] getPozicijeStrings() {
		return this.pozicija;
	}

	public String getPrviKlub() {
		return this.prviKlub;
	}
	
	public String getPozicijeString() {
		String pozicijaString = "";
		for(int i = 0; i < this.trenutniBroj;i++) {
			pozicijaString = pozicijaString + this.pozicija[i] + ",";
		}
		return pozicijaString;
	}

	public void print() {
		System.out.println("Ime i prezime fudbalera: " + this.imeIPrezime);
		System.out.println("Rodjen: " + this.godinaRodjenja);
		System.out.println("Menadzer: " + this.menadzer);
		System.out.println("Prvi klub: " + this.prviKlub);
		System.out.println("Igraju pozicije: ");
		for (int i = 0; i < this.trenutniBroj; i++)
			System.out.println(this.pozicija[i]);
	}
}
