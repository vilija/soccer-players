import javax.swing.*;

public class Frejm extends JFrame{
	
	public PanelPokreni panelPokreni;
	public PanelSaDugmicima panelSaDugmicima;
	public PanelZaDodajPoziciju panelZaDodajPoziciju;
	public PanelDodaj panelDodaj;
	public PanelPrikazi panelPrikazi;
	public FudbalskiKlub fudbalskiKlub;
	public Fudbaler fudbaler;
	
	public Frejm() {
		fudbalskiKlub = new FudbalskiKlub();
		setLayout(null);
		setTitle("Test");
		setSize(1000, 800);
		
		//Panel
		panelPokreni = new PanelPokreni();
		panelPokreni.setBounds(this.getX(), this.getY(), this.getWidth(), this.getHeight());
		this.add(panelPokreni);
		
		panelSaDugmicima = new PanelSaDugmicima();
		panelSaDugmicima.setBounds(this.getX(), this.getY(), this.getWidth(), this.getHeight()*1/9);
		this.add(panelSaDugmicima);
		
		panelDodaj = new PanelDodaj();
		panelDodaj.setVisible(false);
		panelDodaj.setBounds(panelSaDugmicima.getX(), panelSaDugmicima.getHeight(), this.getWidth(), this.getHeight() * 9/10);
		this.add(panelDodaj);
		
		panelZaDodajPoziciju = new PanelZaDodajPoziciju(this);
		panelZaDodajPoziciju.setVisible(false);
		panelZaDodajPoziciju.setBounds(panelSaDugmicima.getX(), panelSaDugmicima.getY() + this.getHeight() * 1/9, this.getWidth(), this.getHeight() * 8/9);
		this.add(panelZaDodajPoziciju);
		
		panelPrikazi = new PanelPrikazi();
		panelPrikazi.setVisible(false);
		panelPrikazi.setBounds(panelSaDugmicima.getX(), panelSaDugmicima.getY()+this.getHeight()*1/9, this.getWidth(), this.getHeight()*8/9);
		this.add(panelPrikazi);
		
		//Osluskivaci
		OsluskivacZaPanelPokreni osluskivacZaPanelPokreni = new OsluskivacZaPanelPokreni(this);
		panelPokreni.pokreniButton.addActionListener(osluskivacZaPanelPokreni);
		
		OsluskivacZaNazad osluskivacZaNazad = new OsluskivacZaNazad(this);
		panelSaDugmicima.nazadButton.addActionListener(osluskivacZaNazad);
		
		OsluskivacZaDodaj osluskivacZaDodaj = new OsluskivacZaDodaj(this);
		panelSaDugmicima.dodajButton.addActionListener(osluskivacZaDodaj);
		
		OsluskivacZaDalje osluskivacZaDalje = new OsluskivacZaDalje(this);
		panelDodaj.daljeButton.addActionListener(osluskivacZaDalje);
		
		OsluskivacZaDodajPoziciju osluskivacZaDodajPoziciju = new OsluskivacZaDodajPoziciju(this);
		panelZaDodajPoziciju.dodajButton.addActionListener(osluskivacZaDodajPoziciju);
		
		OsluskivacZaDugmeGotovo osluskivacZaDugmeGotovo = new OsluskivacZaDugmeGotovo(this);
		panelZaDodajPoziciju.gotovoButton.addActionListener(osluskivacZaDugmeGotovo);
		
		OsluskivacZaPrikazi osluskivacZaPrikazi = new OsluskivacZaPrikazi(this);
		panelSaDugmicima.prikaziButton.addActionListener(osluskivacZaPrikazi);
		
		OsluskivacZaUcitaj osluskivacZaUcitaj = new OsluskivacZaUcitaj(this);
		panelSaDugmicima.ucitajButton.addActionListener(osluskivacZaUcitaj);
		
		
	}
}
