
public class Node {

	public Node prev;
	public Node next;
	public Fudbaler info;

	public Node() {
		this.prev = null;
		this.next = null;
		this.info = null;

	}

	public Node(Fudbaler info) {
		this.prev = null;
		this.next = null;
		this.info = info;
	}

	public Node(Node prev, Fudbaler info, Node next) {
		this.prev = prev;
		this.info = info;
		this.next = next;
	}

	public Fudbaler print() {
		return this.info;
	}
}
