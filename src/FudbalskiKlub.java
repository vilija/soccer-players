
public class FudbalskiKlub implements Interfejs {

	Node head;
	Node tail;

	public FudbalskiKlub() {

	}

	public boolean isEmpty() {
		return head == null;
	}

	public void addToTail(Fudbaler info) {
		if (isEmpty()) {
			head = tail = new Node(info);
		} else {
			tail.next = new Node(info);
			tail = tail.next;
		}
	}

	public Fudbaler deleteFromHead() {
		if (isEmpty()) {
			System.out.println("List is empty! ");
		}
		Fudbaler el = head.info;
		Node tmp = head;
		if (head == tail) {
			head = null;
			tail = null;
		} else {
			head = head.next;
		}
		tmp = null;
		return el;
	}

	public Fudbaler deleteFromTail() {
		if (isEmpty()) {
			System.out.println("List is empty!");
			return null;
		} else {
			Fudbaler el = head.info;
			if (head == tail) {
				head = tail = null;
			} else {
				tail = tail.prev;
				tail.next = null;
			}
			return el;
		}
	}

	public int numberOfElements() {
		int count = 0;
		for (Node tmp = head; tmp != null; tmp = tmp.next) {
			count++;
		}
		return count;
	}

	@Override
	public void dodaj(Fudbaler f) {
		if (isEmpty()) {
			head = tail = new Node(f);
		} else {
			head = new Node(null, f, head);
			head.next.prev = head;
		}
	}

	@Override
	public Fudbaler obrisiZadatog(String punoIme) {
		System.out.println("Brisemo fudbalera: "+"'"+punoIme+"'");
		Fudbaler pomocni = null;
		for (Node tmp = head; tmp != null; tmp = tmp.next) {
			if (tmp.info.getImeIPrezime().equals(punoIme)) {

				if (tmp == head) {
					pomocni = deleteFromHead();
				} else if (tmp == tail) {
					pomocni = deleteFromTail();
				} else {
					Node current = tmp;
					tmp.prev.next = tmp.next;
					tmp.next.prev = tmp.prev;
					current.prev = null;
					current.next = null;
					pomocni = tmp.info;
				}

			}

		}
		return pomocni;
	}

	@Override
	public void prikaziSveFudbalere() {
		for (Node tmp = head; tmp != null; tmp = tmp.next) {
			tmp.info.print();
		}
	}

}
