import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JLabel;

public class OsluskivacZaPrikazi implements ActionListener {

	public Frejm frejm;

	public OsluskivacZaPrikazi(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.panelPrikazi.removeAll();
		frejm.panelPrikazi.repaint();
		

		frejm.panelPrikazi.setVisible(true);
		frejm.panelZaDodajPoziciju.setVisible(false);
		frejm.panelDodaj.setVisible(false);

		JLabel imeIPrezimeJLabel = new JLabel("Ime i prezime");
		imeIPrezimeJLabel.setBounds(5, 5, frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
		frejm.panelPrikazi.add(imeIPrezimeJLabel);

		JLabel godinarodjenjaJLabel = new JLabel("Godina rodjenja");
		godinarodjenjaJLabel.setBounds(imeIPrezimeJLabel.getX() + frejm.panelPrikazi.offsetW, imeIPrezimeJLabel.getY(),
				frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
		frejm.panelPrikazi.add(godinarodjenjaJLabel);

		JLabel menadzerJLabel = new JLabel("Menadzer");
		menadzerJLabel.setBounds(godinarodjenjaJLabel.getX() + frejm.panelPrikazi.offsetW, godinarodjenjaJLabel.getY(),
				frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
		frejm.panelPrikazi.add(menadzerJLabel);

		JLabel prviKlubJLabel = new JLabel("Prvi Klub");
		prviKlubJLabel.setBounds(menadzerJLabel.getX() + frejm.panelPrikazi.offsetW, menadzerJLabel.getY(),
				frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
		frejm.panelPrikazi.add(prviKlubJLabel);

		JLabel pozicijaJLabel = new JLabel("Pozicije");
		pozicijaJLabel.setBounds(prviKlubJLabel.getX() + frejm.panelPrikazi.offsetW, prviKlubJLabel.getY(),
				frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
		frejm.panelPrikazi.add(pozicijaJLabel);
		int startY = 50;

		JButton dugmeObrisi[] = new JButton[frejm.fudbalskiKlub.numberOfElements()];
		OsluskivacZaObrisi osluskivacZaObrisi[] = new OsluskivacZaObrisi[frejm.fudbalskiKlub.numberOfElements()];
		int br = 0;

		for (Node tmp = frejm.fudbalskiKlub.head; tmp != null; tmp = tmp.next) {

			imeIPrezimeJLabel = new JLabel(tmp.info.getImeIPrezime());
			imeIPrezimeJLabel.setBounds(5, startY, frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
			frejm.panelPrikazi.add(imeIPrezimeJLabel);

			godinarodjenjaJLabel = new JLabel(tmp.info.getGodinaRodjenja());
			godinarodjenjaJLabel.setBounds(imeIPrezimeJLabel.getX() + frejm.panelPrikazi.offsetW,
					imeIPrezimeJLabel.getY(), frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
			frejm.panelPrikazi.add(godinarodjenjaJLabel);

			menadzerJLabel = new JLabel(tmp.info.getMenadzer());
			menadzerJLabel.setBounds(godinarodjenjaJLabel.getX() + frejm.panelPrikazi.offsetW,
					godinarodjenjaJLabel.getY(), frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
			frejm.panelPrikazi.add(menadzerJLabel);

			prviKlubJLabel = new JLabel(tmp.info.getPrviKlub());
			prviKlubJLabel.setBounds(menadzerJLabel.getX() + frejm.panelPrikazi.offsetW, menadzerJLabel.getY(),
					frejm.panelPrikazi.inputW, frejm.panelPrikazi.inputH);
			frejm.panelPrikazi.add(prviKlubJLabel);

			pozicijaJLabel = new JLabel(tmp.info.getPozicijeString());
			pozicijaJLabel.setBounds(prviKlubJLabel.getX() + frejm.panelPrikazi.offsetW, prviKlubJLabel.getY(),
					frejm.panelPrikazi.inputW - 100, frejm.panelPrikazi.inputH);
			frejm.panelPrikazi.add(pozicijaJLabel);
			pozicijaJLabel.setToolTipText(tmp.info.getPozicijeString());

			dugmeObrisi[br] = new JButton("X");
			dugmeObrisi[br].setBounds(pozicijaJLabel.getX() + frejm.panelPrikazi.offsetW - 70, pozicijaJLabel.getY(),
					50, 40);
			frejm.panelPrikazi.add(dugmeObrisi[br]);
			dugmeObrisi[br].setBackground(Color.red);

			osluskivacZaObrisi[br] = new OsluskivacZaObrisi(frejm, tmp.info.getImeIPrezime());

			dugmeObrisi[br].addActionListener(this);
			dugmeObrisi[br].addActionListener(osluskivacZaObrisi[br]);

			startY += 50;
			br++;
		}

	}

}
