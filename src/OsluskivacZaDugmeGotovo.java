import java.awt.event.*;

public class OsluskivacZaDugmeGotovo implements ActionListener {
	public Frejm frejm;

	public OsluskivacZaDugmeGotovo(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.fudbalskiKlub.dodaj(frejm.fudbaler);

		frejm.panelZaDodajPoziciju.setVisible(false);
		frejm.panelDodaj.setVisible(true);

		frejm.panelDodaj.imeIPrezimeField.setText("");
		frejm.panelDodaj.godinaRodjenjaField.setText("");
		frejm.panelDodaj.menadzerField.setText("");
		frejm.panelDodaj.prviKlubField.setText("");

	}

}
