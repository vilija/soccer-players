import java.awt.event.*;

public class OsluskivacZaObrisi implements ActionListener {

	public Frejm frejm;
	public String pozicija;

	public OsluskivacZaObrisi(Frejm frejm, String pozicija) {
		this.frejm = frejm;
		this.pozicija = pozicija;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.fudbalskiKlub.obrisiZadatog(pozicija);

	}

}
