import javax.lang.model.element.NestingKind;
import javax.swing.*;

public class PanelDodaj extends JPanel {

	public int buttonW = 150;
	public int buttonH = 40;
	public int offsetW = buttonW + 100;
	public int inputW = 200;
	public int inputH = 40;
	public int offsetH = inputH + 20;
	
	public JLabel imeIPrezimeJLabel;
	public JLabel godinaRodjenjaJLabel;
	public JLabel menadzerJLabel;
	public JLabel prviKlubJLabel;
	
	public JTextField imeIPrezimeField;
	public JTextField godinaRodjenjaField;
	public JTextField menadzerField;
	public JTextField prviKlubField;
	
	public JButton daljeButton;
	
	public PanelDodaj() {
		
		this.setLayout(null);
		
		//LABEL
		imeIPrezimeJLabel = new JLabel("Ime i prezime");
		imeIPrezimeJLabel.setBounds(20, 20, inputW, inputH);
		this.add(imeIPrezimeJLabel);
		
		godinaRodjenjaJLabel = new JLabel("Godina rodjenja");
		godinaRodjenjaJLabel.setBounds(imeIPrezimeJLabel.getX(), imeIPrezimeJLabel.getY() + offsetH, inputW, inputH);
		this.add(godinaRodjenjaJLabel);
		
		menadzerJLabel = new JLabel("Menadzer");
		menadzerJLabel.setBounds(godinaRodjenjaJLabel.getX(), godinaRodjenjaJLabel.getY() + offsetH, inputW, inputH);
		this.add(menadzerJLabel);
		
		prviKlubJLabel = new JLabel("Menadzer");
		prviKlubJLabel.setBounds(menadzerJLabel.getX(), menadzerJLabel.getY() + offsetH, inputW, inputH);
		this.add(prviKlubJLabel);
		
		//FIELD
		imeIPrezimeField = new JTextField();
		imeIPrezimeField.setBounds(imeIPrezimeJLabel.getX() + offsetW, imeIPrezimeJLabel.getY(), inputW, inputH);
		this.add(imeIPrezimeField);
		
		godinaRodjenjaField = new JTextField();
		godinaRodjenjaField.setBounds(imeIPrezimeField.getX(), imeIPrezimeField.getY() + offsetH, inputW, inputH);
		this.add(godinaRodjenjaField);
		
		menadzerField = new JTextField();
		menadzerField.setBounds(godinaRodjenjaField.getX(), godinaRodjenjaField.getY() + offsetH, inputW, inputH);
		this.add(menadzerField);
		
		prviKlubField = new JTextField();
		prviKlubField.setBounds(menadzerField.getX(), menadzerField.getY() + offsetH, inputW, inputH);
		this.add(prviKlubField);
		
		daljeButton = new JButton("Dalje");
		daljeButton.setBounds(prviKlubJLabel.getX(), prviKlubJLabel.getY() + offsetH, buttonW, buttonH);
		this.add(daljeButton);
	}	
}
