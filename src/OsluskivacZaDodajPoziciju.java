import java.awt.event.*;

public class OsluskivacZaDodajPoziciju implements ActionListener {
	
	public Frejm frejm;
	
	public OsluskivacZaDodajPoziciju(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.fudbaler.push(frejm.panelZaDodajPoziciju.pozicijaField.getText());
		frejm.panelZaDodajPoziciju.pozicijaField.setText("");
	}

}
