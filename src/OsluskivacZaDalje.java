import java.awt.event.*;

public class OsluskivacZaDalje implements ActionListener {

	public Frejm frejm;

	public OsluskivacZaDalje(Frejm frejm) {
		this.frejm = frejm;
	}
	
	/*
	public OsluskivacZaDalje(Frejm frejm) {
		this.frejm = frejm;
	}
	*/

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.panelDodaj.setVisible(false);
		frejm.panelZaDodajPoziciju.setVisible(true);
		frejm.fudbaler = new Fudbaler(frejm.panelDodaj.imeIPrezimeField.getText(),
				frejm.panelDodaj.godinaRodjenjaField.getText(), frejm.panelDodaj.menadzerField.getText(), 20,
				frejm.panelDodaj.prviKlubField.getText());

	}

}
