import java.awt.event.*;

public class OsluskivacZaDodaj implements ActionListener {
	
	public Frejm frejm;
	
	public OsluskivacZaDodaj(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.panelDodaj.setVisible(true);
		
	}

}
