import java.awt.event.*;

public class OsluskivacZaPanelPokreni implements ActionListener {

	public Frejm frejm;

	OsluskivacZaPanelPokreni(Frejm frejm) {
		this.frejm = frejm;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		frejm.panelSaDugmicima.setVisible(true);
		frejm.panelZaDodajPoziciju.setVisible(false);
		frejm.panelPokreni.setVisible(false);

	}

}
